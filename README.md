# Validating a Spring (Boot) Response Matches JSON Schema with MockMVC

An example repo to go with [_Validating a Spring (Boot) Response Matches JSON Schema with MockMVC_](), and based on [_Creating a Spring Boot controller, using Test Driven Development_](https://gitlab.com/jamietanna/spring-boot-controller-tdd).
